# Global configuration
BUILD_DIR := $(CURDIR)/_build

# Ocaml configuration
OCAML_SOURCES := $(wildcard lib/*.ml[yli] lib/*.ml)
OCAML_PACKAGES := core async async_shell
SOURCE_DIR := lib

NATIVE_BINARIES := screen_control
NATIVE_BINARY_FILES := $(addsuffix .byte, $(NATIVE_BINARIES))
NATIVE_BINARY_TARGETS := $(addprefix _build/lib/, $(NATIVE_BINARY_FILES))

UTOP_MODS := $(foreach PACKAGE,$(OCAML_PACKAGES),\#require \"$(PACKAGE)\";;)
UTOP_INIT := $(BUILD_DIR)/init.ml

# Nix configuration
NIX_DIR := $(CURDIR)/nix
NIX_EXPRESSIONS := $(NIX_DIR)/bootstrap.nix
NIX_DERIVATION := samtools
NIX_GC_ROOT_DIR := $(BUILD_DIR)/nix-gc-roots

.PHONY: clean shell

all: build

$(BUILD_DIR):
	mkdir $@

build: $(NATIVE_BINARY_TARGETS) $(BUILD_DIR)

$(NATIVE_BINARY_TARGETS): $(OCAML_SOURCES)
	ocamlbuild \
          -j 4 \
          -classic-display \
          -build-dir $(BUILD_DIR) \
          -use-ocamlfind \
          -use-menhir \
          -cflags -bin-annot \
          -lflags -g \
          $(notdir $@)

clean:
	rm -rf $(BUILD_DIR)

utop: $(UTOP_INIT)
	utop -I $(SOURCE_DIR) -init $(UTOP_INIT)

.merlin: build
	trv build make-dot-merlin \
		--build-dir $(BUILD_DIR) \
		--lib "$(OCAML_PACKAGES)" \
		--source-dir $(SOURCE_DIR)

$(UTOP_INIT): $(BUILD_DIR)
	@echo '#use "topfind";;' > $(UTOP_INIT)
	@echo '#directory "$(BUILD_DIR)/lib";;' >> $(UTOP_INIT)
	@echo "$(UTOP_MODS)" >> $(UTOP_INIT)
	@echo "open Core.Std;;" >> $(UTOP_INIT)
	@echo "open Async.Std;;" >> $(UTOP_INIT)


shell:
	nix-shell \
		--pure \
		--indirect --add-root $(NIX_GC_ROOT_DIR) \
		-A $(NIX_DERIVATION) \
		$(NIX_EXPRESSIONS)
