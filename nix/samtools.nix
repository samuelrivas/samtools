{ async,
  async_shell,
  core,
  findlib,
  menhir,
  ocaml,
  sexplib,
  stdenv,
  xrandr,

  # For debugging, not needed for building
  debug ? false,
  emacs ? null,
  man ? null,
  merlin ? null,
  ocpIndent ? null,
  trv ? null,
  utop ? null,
  which ? null
 }:

let
  debugBuildInputs =
    if debug then
      [ emacs merlin ocpIndent trv utop which man ]
    else
      [ ];
  ocaml_version = (builtins.parseDrvName ocaml.name).version;
in
stdenv.mkDerivation rec {
  name = "samtools-0.0.0";

  src = ./..;

  buildInputs = [ async async_shell core findlib menhir ocaml sexplib xrandr ]
                ++ debugBuildInputs;

  # For debug mode only
  OCAML_TOPLEVEL_PATH="${findlib}/lib/ocaml/${ocaml_version}/site-lib";

  meta = {
    description = "A collection of barely useful commands";
    homepage = https://github.com/samuelrivas/samtools;
    license = stdenv.lib.licenses.bsd3;
    maintainers = [ "Samuel Rivas <samuelrivas@gmail.com>" ];
  };
}
