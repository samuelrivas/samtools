{ system ? builtins.currentSystem }:
let
  pkgs = import <nixpkgs> { inherit system; };

  callPackage =
    pkgs.lib.callPackageWith (  pkgs
                             // pkgs.xlibs
                             // pkgs.ocamlPackages_4_02_1
                             // self);

  self = rec {
    samtools = callPackage ./samtools.nix {
      debug = true;
    };
  };
in
self
