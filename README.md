Samtools
========

## Building with nix

Some longer explanation will be written here when this is barely usable, for now
these are just basic instructions since we use some forks here and there.

You'll need a nixpkgs fork that contains:

  * ocaml 4.02 compiled core and async libraries
  * trv

At the time of writing these lines, that is only possible using [Afiniate's
fork](https://github.com/afiniate/nixpkgs) (tested with sha
d321160599a46ed74755dd5bdac36f3aac9775e5).

If you want a shell with everything needed to compile (and a few more utils) run

     AFINIATE_NIXPKGS=/some/path/of/your/choice
     cd $AFINIATE_NIXPKGS
     git clone git@github.com:afiniate/nixpkgs.git
     NIX_PATH=$AFINIATE_NIXPKGS nix-shell --pure -A samtools nix/bootstrap.nix

Don't rename the directory where your repository is downloaded, it has to be
named `nixpkgs` (that is `ls $AFINIATE_NIXPKGS` must show `nixpkgs`)
