open Core.Std
open Async.Std

let print_position outx lexbuf =
  let pos = lexbuf.Lexing.lex_curr_p in
  fprintf outx "%s:%d:%d" pos.Lexing.pos_fname
    pos.Lexing.pos_lnum (pos.Lexing.pos_cnum - pos.Lexing.pos_bol + 1)

let map_if_error f = function
  | Ok x -> Ok x
  | Error x -> Error (f x)

let xrandr () =
  try_with (
    fun() ->
      Async_shell.run_full ~use_extra_path:false "xrandr" []
  )

let print_position () lexbuf =
  let pos = lexbuf.Lexing.lex_curr_p in
  sprintf "%s:%d:%d" pos.Lexing.pos_fname
    pos.Lexing.pos_lnum (pos.Lexing.pos_cnum - pos.Lexing.pos_bol + 1)

let scan_string s =
  let lexbuf = Lexing.from_string s in
  let next_token = fun lexbuf -> Xrandr_lexer.scan lexbuf in
  let rec scan_all lexbuf =
    match next_token lexbuf with
    | Xrandr_parser.EOF -> [Xrandr_parser.EOF]
    | x -> x::scan_all lexbuf in
  scan_all lexbuf

let parse_string s =
  let lexbuf = Lexing.from_string s in
  Xrandr_parser.xrandr_out Xrandr_lexer.scan lexbuf

let parse_with_error s =
  let lexbuf = Lexing.from_string s in
  try Ok (Xrandr_parser.xrandr_out Xrandr_lexer.scan lexbuf) with
  | Xrandr_lexer.Lexer_error err ->
    Error (sprintf "%a: %s\n" print_position lexbuf "foo")
  | Xrandr_parser.Error ->
    Error (sprintf "%a: syntax error\n" print_position lexbuf)

let main () =
  xrandr ()
  >>=? (fun output -> printf "%s\n" output; Deferred.Result.return ())
  >>= function
  | Ok () -> return ()
  | Error _ -> printf "something went wrong\n"; return ()

let command =
  Command.async_basic ~summary:"Manages screens as Samuel likes"
    Command.Spec.empty main

(* let () =
  command
   |> Command.run *)
