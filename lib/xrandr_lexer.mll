{
  open Core.Std
  open Xrandr_parser

  type lexer_error =
      Unknown_input of String.t with sexp

  exception Lexer_error of lexer_error
}

let digit   = ['0'-'9']
let digits  = ['0'-'9']+
let int     = '-'? digits
let float   = '-'? digits '.' digits
let white   = [' ' '\t' '\r' ]+
let times   =  "x"
let current = "current"
let minimum = "minimum"
let maximum = "maximum"
let screen  = "Screen"
let word    = ['a'-'z' 'A' - 'Z']+

rule scan =
  parse
  | int     { INT (Int.of_string (Lexing.lexeme lexbuf)) }
  | float   { FLOAT (Float.of_string (Lexing.lexeme lexbuf)) }
  | screen  { SCREEN }
  | white   { scan lexbuf }
  | current { CURRENT }
  | minimum { MINIMUM }
  | maximum { MAXIMUM }
  | times   { TIMES }
  | ':'     { COLON }
  | ','     { COMMA }
  | '+'     { PLUS }
  | '*'     { STAR }
  | '('     { LEFT_PARENTHESIS }
  | ')'     { RIGHT_PARENTHESIS }
  | '\n'    { NEW_LINE }
  | word    { WORD (Lexing.lexeme lexbuf) }
  | _       { raise @@ Lexer_error (Unknown_input (Lexing.lexeme lexbuf)) }
  | eof     { EOF }
