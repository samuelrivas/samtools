(* This module exports types to represent the useful information output by
   xrandr when called without parameters. The entry point is t, which represents a
   screen. A screen will have a number of outputs, which can be connected or
   disconnected *)

open Core.Std

module type Resolution = sig
  type t
  val make      : width:Int.t -> height:Int.t -> t
  val w         : t -> Int.t
  val h         : t -> Int.t
  val sexp_of_t : t -> Sexp.t
end

module type Position = sig
  type t
  val make      : x:Int.t -> y:Int.t -> t
  val x         : t -> Int.t
  val y         : t -> Int.t
  val sexp_of_t : t -> Sexp.t
end

module Resolution : Resolution = struct
  type t = Int.t * Int.t with sexp
  let make ~width ~height = (width, height)
  let w = fst
  let h = snd
end

module Position : Position = struct
  type t = Int.t * Int.t with sexp
  let make ~x ~y = (x, y)
  let x = fst
  let y = snd
end

module Output = struct
  type connected = {
    resolution : Resolution.t;
    position : Position.t;
    primary : Bool.t;
  }

  type status =
    | Connected of connected
    | Disconnected

  type t = {
    id : String.t;
    o_type : String.t;
    status : status;
  }
end

module Screen_description = struct
  type t = {
    min_resolution : Resolution.t;
    current_resolution : Resolution.t;
    max_resolution : Resolution.t;
    outputs : Output.t list;
  } with fields
end

module Screen = struct
  type t = {
    id : String.t;
    description : Screen_description.t;
  }
end

type config = Screen.t
