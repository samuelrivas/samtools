%token <Core.Std.Int.t> INT
%token <Core.Std.Float.t> FLOAT
%token <Core.Std.String.t> WORD
%token COLON
%token COMMA
%token CURRENT
%token EOF
%token LEFT_PARENTHESIS
%token MAXIMUM
%token MINIMUM
%token NEW_LINE
%token PLUS
%token RIGHT_PARENTHESIS
%token SCREEN
%token STAR
%token TIMES

%start <Xrandr.Screen.t> xrandr_out
%%

xrandr_screen:
  | SCREEN; id = INT; COLON; description = screen_description {
      { Xrandr.Screen.id = Core.Std.Int.to_string id;
        Xrandr.Screen.description = description;
      }
   } ;

screen_description:
  | min = min_resolution; COMMA; current = current_resolution; COMMA; max = max_resolution {
      Xrandr.Screen_description.Fields.create
        ~min_resolution:min
        ~current_resolution:current
        ~max_resolution:max
        ~outputs:[]
    };

min_resolution:
  | MINIMUM; r = resolution { r };

current_resolution:
  | CURRENT; r = resolution { r };

max_resolution:
  | MAXIMUM; r = resolution { r };


resolution:
  | w = INT; TIMES; h = INT { Xrandr.Resolution.make ~width:w ~height:h };

xrandr_out:
  | xrandr_screen NEW_LINE { $1 }
